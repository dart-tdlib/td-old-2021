package dev.pqhaz.td

import android.os.Handler
import android.os.Looper
import androidx.annotation.NonNull
import io.flutter.Log

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.*
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import org.drinkless.tdlib.JsonClient
import org.json.JSONObject

/** TdPlugin */
class TdPlugin : FlutterPlugin, MethodCallHandler {
    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private lateinit var channel: MethodChannel
    private lateinit var eventsChannel: EventChannel
    private lateinit var flutterBindings: FlutterPlugin.FlutterPluginBinding
    private var eventChannelManager: EventChannelManager = EventChannelManager()

    private var clientThreads: MutableMap<Long, Thread> = mutableMapOf()

    data class ErrorMessage(
        val errorCode: String,
        val errorMessage: String,
        val errorDetails: Object
    )

    class EventChannelManager : EventChannel.StreamHandler {
        private var eventSink: EventChannel.EventSink? = null
        private var eventSuccess: MutableList<Any> = mutableListOf()
        private var eventsError: MutableList<ErrorMessage> = mutableListOf()

        override fun onListen(arguments: Any?, eventSinkArg: EventChannel.EventSink?) {
            eventSink = eventSinkArg
            for (event in eventSuccess) {
                eventSinkArg!!.success(event)
            }

            for (event in eventsError) {
                eventSinkArg!!.error(event.errorCode, event.errorMessage, event.errorDetails)
            }
        }

        override fun onCancel(arguments: Any?) {
            eventSink = null;
        }

        fun success(event: Any) {
            if (eventSink == null) {
                eventSuccess.add(event)
            } else {
                eventSink!!.success(event)
            }
        }

        fun error(event: ErrorMessage) {
            if (eventSink == null) {
                eventsError.add(event)
            } else {
                eventSink!!.error(event.errorCode, event.errorMessage, event.errorDetails)
            }
        }
    }

    class ClientEventsThread(val clientId: Long, val eventManager: EventChannelManager) : Thread() {
        override fun run() {
            while (true) {
                if (!interrupted()) {
                    val response: String? = JsonClient.receive(clientId, 1.0);
                    if (response != null) {
                        val jsonObject = JSONObject(response)
                        jsonObject.put("clientId", clientId)
                        Handler(Looper.getMainLooper()).post {
                            eventManager.success(jsonObject.toString())
                        }
                    }
                } else {
                    break
                }
            }
        }
    }

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        eventsChannel = EventChannel(flutterPluginBinding.binaryMessenger, "td/events");
        eventsChannel.setStreamHandler(eventChannelManager)
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "td")
        channel.setMethodCallHandler(this)
        flutterBindings = flutterPluginBinding;
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        if (call.method == "clientCreate") {
            if ((call.argument("shouldCleanClients") as Boolean?) == true) {
                for ((clientId, clientThread) in clientThreads) {
                    clientThread.interrupt();
                    clientThread.join();
                    JsonClient.destroy(clientId);
                }
            }

            var clientId = JsonClient.create();
            clientThreads[clientId] = ClientEventsThread(clientId, eventChannelManager);
            clientThreads[clientId]?.start();
            result.success(clientId);
        } else if (call.method == "clientDestroy") {
            var clientId = call.argument("clientId") as Long?;

            if (clientId != null && clientThreads[clientId] != null) {
                clientThreads[clientId]?.interrupt()
                clientThreads[clientId]?.join()
                JsonClient.destroy(clientId)
                result.success(true);
            } else {
                result.error("ERROR", "NOT_FOUND", "Client not found")
            }
        } else if (call.method == "clientSend") {
            var clientId = call.argument("clientId") as Long?;

            if (clientId != null) {
                JsonClient.send(clientId, call.argument("query") as String?); result.success(null)
            } else {
                result.error("ERROR", "NOT_FOUND", "Client not found")
            }
        } else if (call.method == "clientExecute") {
            var clientId = call.argument("clientId") as Long?;

            if (clientId != null) {
                val reqResult = JsonClient.execute(clientId, call.argument("query") as String?);
                result.success(reqResult);
            } else {
                result.error("ERROR", "NOT_FOUND", "Client not found")
            }
        } else {
            result.notImplemented()
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }
}
