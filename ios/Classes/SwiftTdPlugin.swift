import Flutter
import UIKit
import TDLibFramework

struct ErrorMessage {
    var errorCode: String
    var errorMessage: String
    var errorDetails: NSObject
}

class EventChannelManager : NSObject, FlutterStreamHandler {
    private var mEventSink: FlutterEventSink?
    private var eventSuccess: NSMutableArray = NSMutableArray()
    private var eventsError: NSMutableArray = NSMutableArray()
    
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        mEventSink = events
        for event in (eventSuccess) {
            mEventSink!(event)
        }
        for event in (eventsError as! [ErrorMessage]) {
            mEventSink!(FlutterError(code: event.errorCode, message: event.errorMessage, details: event.errorDetails))
        }
        return nil
    }
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        mEventSink = nil
        return nil
    }
    
    func success(event: Any) {
        if (mEventSink == nil) {
            eventSuccess.add(event)
        } else {
            mEventSink!(event)
        }
    }
    
    func error(event: ErrorMessage) {
        if (mEventSink == nil) {
            eventsError.add(event)
        } else {
            mEventSink!(FlutterError(code: event.errorCode, message: event.errorMessage, details: event.errorDetails))
        }
    }
}



// Must be there :(
var mEventsChannelManager: EventChannelManager = EventChannelManager()

class ClientWrapper {
    private var client: UnsafeMutableRawPointer!
    private let tdlibMainQueue: DispatchQueue
    private let tdlibQueryQueue: DispatchQueue
    private var isClientDestroyed = true
    private var stopFlag = false
    public var clientId: Int
    
    public init() {
        self.client = td_json_client_create()
        self.isClientDestroyed = false;
        self.clientId = Int.random(in: 0...999) // We don't really care
        self.tdlibMainQueue = DispatchQueue(label: "TDLib-" + String(self.clientId), qos: .utility)
        self.tdlibQueryQueue = DispatchQueue(label: "TDLibQuery-" + String(self.clientId), qos: .userInitiated)
    }

    deinit {
        close()
    }
    
    public func close() {
        guard !isClientDestroyed else { return }
        isClientDestroyed = true
        td_json_client_destroy(client)
    }
    
    public func run() {
        tdlibMainQueue.async { [unowned self] in
            guard !self.isClientDestroyed else { return }
            
            while (!self.stopFlag) {
                guard
                    let res = td_json_client_receive(self.client, 1),
                    let data = String(cString: res).data(using: .utf8)
                else {
                    continue
                }

                self.queryResultAsync(data)
            }
        }
    }
    
    private func queryResultAsync(_ result: Data) {
        tdlibQueryQueue.async { [weak self] in
            guard
                let `self` = self,
                let json = try? JSONSerialization.jsonObject(with: result, options:[]),
                var dictionary = json as? [String:Any]
            else {
                return
            }
                
            dictionary.merge(zip(["clientId"], [self.clientId])) { (_, new) in new }
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
                let stringResp = String(bytes: jsonData, encoding: String.Encoding.utf8)
                mEventsChannelManager.success(event: stringResp!)
            } catch {
                NSLog("Error while doing things with JSON")
            }
        }
    }
    
    public func send(query: String) {
        guard !self.isClientDestroyed else { return }
        
        tdlibQueryQueue.async { [weak self] in
            guard let `self` = self else { return }
            td_json_client_send(self.client, query)
        }
    }
    
    public func execute(query: String) -> String? {
        guard !self.isClientDestroyed else { return nil }
        
        if let res = td_json_client_execute(client, query) {
            let resString = String(cString: res)
            return resString
        } else {
            return nil
        }
    }
}

public class SwiftTdPlugin: NSObject, FlutterPlugin {
    private var mChannel: FlutterMethodChannel?
    private var mEventsChannel: FlutterEventChannel?
    private var clientThreads: [Int:ClientWrapper] = [:]
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "td", binaryMessenger: registrar.messenger())
        let statusEventChannel = FlutterEventChannel(name: "td/events", binaryMessenger: registrar.messenger())
        statusEventChannel.setStreamHandler(mEventsChannelManager)
        let instance = SwiftTdPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    let args = call.arguments as! [String: Any]
    if (call.method == "clientCreate") {
        if ((args["shouldCleanClients"] as! Bool?) == true) {
            for (_, clientThread) in clientThreads {
                clientThread.close()
            }
        }
        
        let client = ClientWrapper()
        clientThreads[client.clientId] = client
        clientThreads[client.clientId]!.run()
        result(client.clientId)
    } else if (call.method == "clientDestroy") {
        let clientId = args["clientId"] as! Int

        if (clientThreads[clientId] != nil) {
            clientThreads[clientId]!.close()
            result(true)
        } else {
            result(FlutterError(code: "ERROR", message: "ERROR", details: "Client not found"))
        }
    } else if (call.method == "clientSend") {
        let clientId = args["clientId"] as! Int
        let query = args["query"] as! String

        if (clientThreads[clientId] != nil) {
            clientThreads[clientId]!.send(query: query)
            result(true)
        }
    } else if (call.method == "clientExecute") {
        let clientId = args["clientId"] as! Int
        let query = args["query"] as! String
        
        if (clientThreads[clientId] != nil) {
            let res = clientThreads[clientId]!.execute(query: query)
            result(res)
        }
    } else {
        result(false)
    }
  }
}
