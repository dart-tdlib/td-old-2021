# td

[![pub package](https://img.shields.io/pub/v/td.svg)](https://pub.dev/packages/td) [![pub points](https://badges.bar/td/pub%20points)](https://pub.dev/packages/td/score) [![popularity](https://badges.bar/td/popularity)](https://pub.dev/packages/td/score) [![likes](https://badges.bar/td/likes)](https://pub.dev/packages/td/score)

Flutter wrapper for [TDLib](https://github.com/tdlib/td). 
On Android it's possible to use platform channels (JNI) implementation and direct FFI, on iOS only platform channels implementation via FFI.

## Supported architectures

Make sure you are using supported one

| Platform         | Architecture |            |
| ---------------- | ------------ | ---------- |
| Android          | armeabi-v7a  | ✅         |
|                  | arm64-v8a    | ✅         |
| Android emulator | x86          | ✅         |
|                  | x86_64       | ✅         |
| iOS              | armv7        | ✅         |
|                  | armv7s       | ✅         |
|                  | arm64        | ✅         |
| iOS simulator    | i386         | ✅         |
|                  | x86_64       | ✅         |
|                  | arm64 (M1)   | ✅         |
| macOS            | i386         | ❌ [WIP]   |
|                  | x86_64       | ❌ [WIP]   |
|                  | arm64 (M1)   | ❌ [WIP]   |
| Web              | -         | ❌ [WIP]   |

## Install

- Update `pubspec.yaml`:

  ```yml
  dependencies:
    td: ^1.0.0
  ```

- Please check the example and try running it before opening issues.

- To use on Android:
    Clone the library with submodules, android prebuilts are included via submodule.
    The library is using platform channels (JNI) implementation by default, to change it pass `Backend.FFI` while calling `TelegramService().initClient`, but note that it's unstable and hot restart might not work properly, I'll work on that later.

- To use on iOS:
    Just run the app with Xcode once I guess, or maybe it isn't required it all.
    The library uses [TDLibFramework](https://github.com/Swiftgram/TDLibFramework) by [Sergey](https://github.com/Kylmakalle), make sure to give a star on his repo!

- macOS and Web isn't available yet, I'll add their support later. macOS will come first, then web.
- P.S. This readme is heavily based on [libtdjson](https://pub.dev/packages/libtdjson) repo, thanks to its author.
- How to read package version?
  The main version is version of the wrapper itself, subversion (which comes after +) is the TDLib version included in prebuilts.
